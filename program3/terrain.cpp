#include "terrain.h"

using glm::normalize;
using glm::length;
using glm::cross;

Terrain::Terrain(int in_width, int in_length) {
    width = in_width;
    length = in_length;

    heights = new float*[length];
    for(int i = 0; i < length; i++) {
       heights[i] = new float[width];
    }

    normals = new vec3*[length];
    for(int i = 0; i < length; i++) {
       normals[i] = new vec3[width];
    }

    computedNormals = false;
}

Terrain::~Terrain() {
    for(int i = 0; i < length; i++) {
        delete[] heights[i];
    }
    delete[] heights;

    for(int i = 0; i < length; i++) {
        delete[] normals[i];
    }
    delete[] normals;
}

void Terrain::setHeight(int x, int z, float y) {
    heights[z][x] = y;
    computedNormals = false;
}

float Terrain::getHeight(int x, int z) {
    return heights[z][x];
}

vec3 Terrain::getNormal(int x, int z) {
    if (!computedNormals) {
        computeNormals();
    }
    return normals[z][x];
}

void Terrain::computeNormals() {
    if (computedNormals) {
        return;
    }

    vec3** normals2 = new vec3*[length];
    for(int i = 0; i < length; i++) {
        normals2[i] = new vec3[width];
    }

    for(int z = 0; z < length; z++) {
        for(int x = 0; x < width; x++) {
            vec3 sum(0.0f, 0.0f, 0.0f);

            vec3 out;
            if (z > 0) {
                out = vec3(0.0f, heights[z - 1][x] - heights[z][x], -1.0f);
            }
            vec3 in;
            if (z < length - 1) {
                in = vec3(0.0f, heights[z + 1][x] - heights[z][x], 1.0f);
            }
            vec3 left;
            if (x > 0) {
                left = vec3(-1.0f, heights[z][x - 1] - heights[z][x], 0.0f);
            }
            vec3 right;
            if (x < width - 1) {
                right = vec3(1.0f, heights[z][x + 1] - heights[z][x], 0.0f);
            }

            if (x > 0 && z > 0) {
                sum += glm::normalize(glm::cross(out, left));
            }
            if (x > 0 && z < length - 1) {
                sum += glm::normalize(glm::cross(left, in));
            }
            if (x < width - 1 && z < length - 1) {
                sum += glm::normalize(glm::cross(in, right));
            }
            if (x < width - 1 && z > 0) {
                sum += glm::normalize(glm::cross(right, out));
            }

            normals2[z][x] = glm::normalize(sum);
        }
    }

    normals=normals2;
}


