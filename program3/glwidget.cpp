#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>
#include <algorithm>    // std::max
#include "imageloader.h"
#include "terrain.h"
#include <QDateTime>
#pragma comment(lib,"opengl32.lib")

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::translate;
using glm::value_ptr;
using glm::lookAt;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(animate()));
    timer->start(16);
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeTerrain() {
    glGenVertexArrays(1, &terrVao);
    glBindVertexArray(terrVao);

    // Create a buffer on the GPU for position data
    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);

    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    terrain = GLWidget::loadTerrain("another.bmp", 1);

    const int ptsSize = terrain->width * terrain->length;
    std::cout << "Terrain [" << terrain->width << " " << terrain->length << "]" <<std::endl;

    vec3 pts[10000];
    vec3 normals[10000];

    int k = 0;
    for(int i = 0; i < renderedPixels - 1; i++) {
        for(int j = 0; j < renderedPixels - 1; j++) {
            pts[k++] = GLWidget::terrainPoint(i, j);
//            normals[k] = GLWidget::normal(i, j);

            pts[k++] = GLWidget::terrainPoint(i + 1, j);
//            normals[k] = GLWidget::normal(i + 1, j);
            pts[k++] = GLWidget::terrainPoint(i, j + 1);
//            normals[k] = GLWidget::normal(i, j + 1);

            pts[k++] = GLWidget::terrainPoint(i + 1, j);
//            normals[k] = GLWidget::normal(i + 1, j);
            pts[k++] = GLWidget::terrainPoint(i, j + 1);
//            normals[k] = GLWidget::normal(i, j + 1);
            pts[k++] = GLWidget::terrainPoint(i + 1, j + 1);
//            normals[k] = GLWidget::normal(i + 1, j + 1);
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_DYNAMIC_DRAW);

//    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);



    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/terr_vert.glsl", ":/terr_frag.glsl");
    glUseProgram(program);
    terrProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    terrProjMatrixLoc = glGetUniformLocation(program, "projection");
    terrViewMatrixLoc = glGetUniformLocation(program, "view");
    terrModelMatrixLoc = glGetUniformLocation(program, "model");
}


vec3 GLWidget::normal(int x, int y) {
    return terrain->getNormal(x,y);
}

void GLWidget::initializeCube() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    vec3 pts[] = {

        // top
        vec3(1,1,1),    // 0
        vec3(1,1,-1),   // 1
        vec3(-1,1,-1),  // 2
        vec3(-1,1,1),   // 3

        // bottom
        vec3(1,-1,1),   // 4
        vec3(-1,-1,1),  // 5
        vec3(-1,-1,-1), // 6
        vec3(1,-1,-1),  // 7

        // front
        vec3(1,1,1),    // 8
        vec3(-1,1,1),   // 9
        vec3(-1,-1,1),  // 10
        vec3(1,-1,1),   // 11

        // back
        vec3(-1,-1,-1), // 12
        vec3(-1,1,-1),  // 13
        vec3(1,1,-1),   // 14
        vec3(1,-1,-1),  // 15

        // right
        vec3(1,-1,1),   // 16
        vec3(1,-1,-1),  // 17
        vec3(1,1,-1),   // 18
        vec3(1,1,1),     // 19

        // left
        vec3(-1,-1,1),  // 20
        vec3(-1,1,1),   // 21
        vec3(-1,1,-1),  // 22
        vec3(-1,-1,-1), // 23
    };

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 normals[] = {

    // top
    vec3(0, 1, 0),
    vec3(0, 1, 0),
    vec3(0, 1, 0),
    vec3(0, 1, 0),

    // bottom
    vec3(0, -1, 0),
    vec3(0, -1, 0),
    vec3(0, -1, 0),
    vec3(0, -1, 0),

    // front
    vec3(0, 0, 1),
    vec3(0, 0, 1),
    vec3(0, 0, 1),
    vec3(0, 0, 1),

    // back
    vec3(0, 0, -1),
    vec3(0, 0, -1),
    vec3(0, 0, -1),
    vec3(0, 0, -1),

    // right
    vec3(1, 0, 0),
    vec3(1, 0, 0),
    vec3(1, 0, 0),
    vec3(1, 0, 0),

    // left
    vec3(-1, 0, 0),
    vec3(-1, 0, 0),
    vec3(-1, 0, 0),
    vec3(-1, 0, 0)
    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    cubeProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // From lab7
    cubeProjMatrixLoc = glGetUniformLocation(program, "projection");
    cubeViewMatrixLoc = glGetUniformLocation(program, "view");
    cubeModelMatrixLoc = glGetUniformLocation(program, "model");
    cubeAnimationMatrixLoc = glGetUniformLocation(program, "animation");

    cubeDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    cubeAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    cubeLightPosLoc = glGetUniformLocation(program, "lightPos");
    cubeLightColorLoc = glGetUniformLocation(program, "lightColor");
    cubeLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    glUniform3f(cubeLightPosLoc, 12, 100, 30);
    glUniform3f(cubeLightColorLoc, .5f, .6f, 1);
    glUniform1f(cubeLightIntensityLoc, .8f);

    glUniform3f(cubeAmbientColorLoc, clearColor.x + .2f, clearColor.y + .02f, clearColor.z + 0.013f);
    glUniform3f(cubeDiffuseColorLoc, clearColor.x + 0.15f, clearColor.y + 0.02f, clearColor.z + 0.013f);
}

void GLWidget::initializeBrick() {
    clrA = 0.2f;
    clrS = 0.2f;
    clrD = 0.2f;
}

void GLWidget::initializeGL() {

    initializeOpenGLFunctions();

    clearColor = vec3(0.2f, 0.00f, 0.00f);
    glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);


    glPointSize(4.0f);

    ticks = 0;
    time = 0;
    curNumBricks = 40;
    gravity = vec3(0, -0.7f, 0);
    shiftActive = 0;

    done = false;

    rotations.push_back(vec3(0, 0, 0));
    rotations.push_back(vec3(0, M_PI, 0));
    rotations.push_back(vec3(0, 2 * M_PI, 0));

    // First Person Initializations
    characterHeight = 2.0f;
    position = vec3(70, 10, 95);
    velocity = vec3(0, 0, 0);

    towerOffset = vec3(30, -4.5f, 20);

    terrainScale = 3;
    initTerrOffset = vec3(-10, -19, -10);
    renderedPixels = 50;

    viewMatrix = mat4(1.0f);

    yawAngle = 0.0f;
    pitchAngle = 0.0f;
    yawMatrix = mat4(1.0f);
    pitchMatrix = mat4(1.0f);
    animationMatrix = mat4(1.0f);
    antiClockWiseAnimationMatrix = mat4(1.0f);

    initializeBrick();
    initializeCube();
    initializeTerrain();
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, 1.0f, 100.0f);
    modelStack.push(mat4(1.0f));
    cubeModelMatrixLoc = glGetUniformLocation(cubeProg, "model");

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelStack.top()));

    glUseProgram(terrProg);
    glUniformMatrix4fv(terrProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(terrModelMatrixLoc, 1, false, value_ptr(modelStack.top()));
}

void GLWidget::paintGL() {
    glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderTower();

    renderTerrain();

    GLWidget::translate(58.0f, -4.5f, 90.0f);
        renderBox();
    GLWidget::pop();
}

void GLWidget::renderBox() {

    updateColors(.4f, .4f, .4f);

    float brickX = 1.0f;
    float brickY = 0.5f;
    float brickZ = brickX / 2;
    float offset = 0.0f;
    curNumBricks = 30 - 2 * ((ticks % 600) / 40);
    int numBricksX = curNumBricks;
    int numBricksY = 40;

    float wallLength = numBricksX * (brickX + offset)
            + (brickX / 2); // For the alternating jutting out bricks

    float halfTranslate = wallLength / 2 - brickX / 2;

    renderWall(flat, 0, brickX, brickY, brickZ, offset, numBricksX, numBricksY);

    GLWidget::translate(0, 0, wallLength);
            renderWall(flat, 0, brickX, brickY, brickZ, offset, numBricksX, numBricksY);
    GLWidget::pop();

    GLWidget::translate(halfTranslate, 0, halfTranslate);
        GLWidget::rotate(float(M_PI) / 2.0f);
            renderWall(flat, 1, brickX, brickY, brickZ, offset, numBricksX, numBricksY);
            GLWidget::translate(0, 0, -wallLength);
                    if(ticks > 400)
                        renderWall(doored, 1, brickX, brickY, brickZ, offset, numBricksX, numBricksY);
                    else
                        renderWall(flat, 1, brickX, brickY, brickZ, offset, numBricksX, numBricksY);
            GLWidget::pop();
        GLWidget::pop();
    GLWidget::pop();
}

void GLWidget::renderTower() {
    float brickX = .6f;
    float brickY = 10.0f;
    float brickZ = brickX / 2;
    float offset = 0.05f;
    int numBricksX = 7;
    int numBricksY = 13;

    vec3 curColor = vec3(0, 0, 0);

    GLWidget::translate(towerOffset.x, towerOffset.y, towerOffset.z);
        for(int i = 0; i < 9; i ++) {
            if(i % 2 == 0)
                GLWidget::transform(animationMatrix);
            else
                GLWidget::transform(antiClockWiseAnimationMatrix);

                GLWidget::updateColors(curColor.x += .16f, curColor.y += .03f, curColor.z += .04f);
                GLWidget::renderWall(flue, 1, brickX += 2.1, brickY - 1, brickZ, offset, numBricksX += 1, numBricksY--);
            GLWidget::pop();

            offset *= 1.1f;
        }
    GLWidget::pop();
}

void GLWidget::updateColors(float x, float y, float z) {
    glUseProgram(cubeProg);
    cubeDiffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    cubeAmbientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");

    glUniform3f(cubeAmbientColorLoc, x, y, z);
    glUniform3f(cubeDiffuseColorLoc, x + .03, y + .02, z + .02);
}

void GLWidget::renderWall(RenderMode renderMode,
                          int options,
                          float brickX,
                          float brickY,
                          float brickZ,
                          float offset,
                          int numBricksX,
                          int numBricksY) {
    switch(renderMode) {
    case flat:
        renderFlatWall(options,
                       brickX,
                       brickY,
                       brickZ,
                       offset,
                       numBricksX,
                       numBricksY);
        break;
    case doored:
        renderDooredWall(options,
                       brickX,
                       brickY,
                       brickZ,
                       offset,
                       numBricksX,
                       numBricksY);
        break;
        case round:
            renderCircle(brickX,
                         brickY,
                         brickZ,
                         offset,
                         numBricksX,
                         numBricksY);
            break;
        case flue:
            renderFlue(brickX,
                       brickY,
                       brickZ,
                       offset,
                       numBricksX,
                       numBricksY);
            break;
    }
}

void GLWidget::renderFlatWall(int offsetConfig,
                                float brickX,
                                float brickY,
                                float brickZ,
                                float offset,
                                int numBricksX,
                                int numBricksY) {
    glUseProgram(cubeProg);

    GLWidget::translate(-numBricksX / 2 * (brickX + offset), 0, 0);

    for(int i = 0; i < numBricksY; i++) {
        bool offsetRow = i % 2 == offsetConfig;

        int extra = 0;
        if(offsetRow) {
            extra = 1;
        }


        GLWidget::translate(0, i * (brickY + offset), 0);
        for(int j = 0; j < numBricksX + extra; j++) {

            float extraBulk = 0.0f;
            if((i + j) % 2 == 0) {
                extraBulk = .1f;
            }

            float translation;
            if(offsetRow) {
                translation = j * (brickX + offset) - (brickX / 2);
            } else {
                translation = j * (brickX + offset);
            }
            GLWidget::translate(translation, 0, 0);
                renderBrick(brickX, brickY, brickZ + extraBulk);
            GLWidget::pop();
        }
        GLWidget::pop();
    }

    GLWidget::pop();
}

void GLWidget::renderDooredWall(int offsetConfig,
                                float brickX,
                                float brickY,
                                float brickZ,
                                float offset,
                                int numBricksX,
                                int numBricksY) {
    glUseProgram(cubeProg);

    GLWidget::translate(-numBricksX / 2 * (brickX + offset), 0, 0);

    for(int i = 0; i < numBricksY; i++) {
        bool offsetRow = i % 2 == offsetConfig;

        int extra = 0;
        if(offsetRow) {
            extra = 1;
        }

        GLWidget::translate(0, i * (brickY + offset), 0);
        for(int j = 0; j < numBricksX + extra; j++) {

            if(j > (numBricksX / 2) - 3 && j < (numBricksX / 2) + 3
                    && i < 9)
                continue; //empty space for door

            float extraBulk = 0.0f;
            if((i + j) % 2 == 0) {
                extraBulk = .1f;
            }

            float translation;
            if(offsetRow) {
                translation = j * (brickX + offset) - (brickX / 2);
            } else {
                translation = j * (brickX + offset);
            }
            GLWidget::translate(translation, 0, 0);
                renderBrick(brickX, brickY, brickZ + extraBulk);
            GLWidget::pop();
        }
        GLWidget::pop();
    }

    GLWidget::pop();
}

void GLWidget::renderCircle(float brickX,
                            float brickY,
                            float brickZ,
                            float offset,
                            int numBricksX,
                            int numBricksY) {
    glUseProgram(cubeProg);
    float radius = (brickX + offset) / (2 * sin(M_PI /numBricksX));
    for(float j = 0; j < numBricksY; j++) {
        for(float i = 0; i < numBricksX; i++) {
            // Do Calculations
            float theta = (i / float(numBricksX)) * 2.0f * float(M_PI);
            float dx = radius * cos(theta);
            float dz = radius * sin(theta);

            //Render Brick at specified coordinates
            GLWidget::translate(dx, j * (brickZ + offset), dz);
                GLWidget::rotate(-theta + M_PI / 2);
                    renderBrick(brickX, brickY, brickZ);
                GLWidget::pop();
            GLWidget::pop();
        }
    }
}

void GLWidget::renderFlue(float brickX,
                          float brickY,
                          float brickZ,
                          float offset,
                          int numBricksX,
                          int numBricksY) {
    glUseProgram(cubeProg);

    float curNumBricks = numBricksX;
    for(float j = 0; j < numBricksY; j++, curNumBricks--) {
        float radius = (brickX + offset) / (2 * sin(M_PI / curNumBricks));
        for(float i = 0; i < curNumBricks; i++) {
            // Do Calculations
            float theta = (i / float(curNumBricks)) * 2.0f * float(M_PI);
            float dx = radius * cos(theta);
            float dz = radius * sin(theta);

            //Render Brick at specified coordinates
            GLWidget::translate(dx, j * (brickY + offset), dz);
                GLWidget::rotate(-theta + M_PI / 2);
                    renderBrick(brickX, brickY, brickZ);
                GLWidget::pop();
            GLWidget::pop();
        }
    }
}

void GLWidget::renderBrick(float brickX, float brickY, float brickZ) {
    GLWidget::translate(0, brickY / 2, 0);
        GLWidget::scale(brickX, brickY, brickZ);
            GLWidget::renderCube();
        GLWidget::pop();
    GLWidget::pop();
}


void GLWidget::translate(float dx, float dy, float dz) {
    glUseProgram(cubeProg);
    modelStack.push(glm::translate(modelStack.top(), vec3(dx, dy, dz)));
    cubeModelMatrixLoc = glGetUniformLocation(cubeProg, "model");
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelStack.top()));
}

void GLWidget::rotate(float angle) {
    glUseProgram(cubeProg);
    modelStack.push(glm::rotate(modelStack.top(), angle, vec3(0, 1, 0)));
    cubeModelMatrixLoc = glGetUniformLocation(cubeProg, "model");
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelStack.top()));
}

// Generic Transformation Method. Called with the animationMatrix
void GLWidget::transform(mat4 transformationMatrix) {
    glUseProgram(cubeProg);
    modelStack.push(modelStack.top() * transformationMatrix);
    cubeModelMatrixLoc = glGetUniformLocation(cubeProg, "model");
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelStack.top()));
}

void GLWidget::scale(float dx, float dy, float dz) {
    modelStack.push(glm::scale(modelStack.top(), vec3(dx, dy, dz)));
    cubeModelMatrixLoc = glGetUniformLocation(cubeProg, "model");
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelStack.top()));
}

void GLWidget::pop() {
    glUseProgram(cubeProg);
    modelStack.pop();
    cubeModelMatrixLoc = glGetUniformLocation(cubeProg, "model");
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelStack.top()));
}

void GLWidget::renderCube() {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderTerrain() {
    glUseProgram(terrProg);
    glBindVertexArray(terrVao);

    terrColorLoc = glGetUniformLocation(terrProg, "color");
    vec4 color = vec4(0, 0, 0.1f, 1);
    glUniform4fv(terrColorLoc, 1, value_ptr(color));
    glDrawArrays(GL_LINES, 0, 50000);

    terrColorLoc = glGetUniformLocation(terrProg, "color");
    color = vec4(.2f, .35f, .22f, 1);
    glUniform4fv(terrColorLoc, 1, value_ptr(color));

    glDrawArrays(GL_TRIANGLES, 0, 50000);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastPt = pt;
    std::cout << lastPt.x << " " << lastPt.y <<std::endl;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec2 d = pt-lastPt;

    // Yaw
    std::cout << "d.x: " << d.x << std::endl;
    yawAngle += (0.001f * -d.x);
    yawMatrix = glm::rotate(mat4(1.0f), yawAngle, vec3(0, 1, 0));
    std::cout << "Yaw: " << yawAngle * 180.0f / 3.1415f << " degrees." << std::endl;

    // Pitch
    std::cout << "d.y: " << d.y << std::endl;
    if(pitchAngle < ((M_PI / 2.0f) - (0.001f * -d.y)) && pitchAngle > -((M_PI / 2.0f) + (0.001f * -d.y))){
        pitchAngle += (0.001f * -d.y);
    }
    pitchMatrix = glm::rotate(mat4(1.0f), pitchAngle, vec3(1, 0, 0));
    std::cout << "Pitch: " << pitchAngle * 180.0f / 3.1415f << " degrees." << std::endl;

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    glUseProgram(terrProg);
    glUniformMatrix4fv(terrViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    lastPt = pt;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    vec3 right = vec3(yawMatrix * vec4(1, 0, 0, 0));
    vec3 forward = vec3(yawMatrix * vec4(0, 0, -1, 0));
    vec3 up = vec3(yawMatrix * vec4(0, 12, 0, 0));

    switch(event->key()) {
        case Qt::Key_W:
        velocity += forward;
            // forward
            break;
        case Qt::Key_A:
        velocity += -right;
            // left
            break;
        case Qt::Key_S:
        velocity += -forward;
            // back
            break;
        case Qt::Key_D:
        velocity += right;
            // right
            break;
        case Qt::Key_Shift:
            shiftActive = true;
            break;
        case Qt::Key_Space:
            velocity += up;
            break;
        case Qt::Key_R:
            velocity = vec3(0, 0, 0);
            position = vec3(70, 10, 95);
            break;

            std::cout<< position.x << " " << position.z << std::endl;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
    vec3 right = vec3(yawMatrix * vec4(1, 0, 0, 0));
    vec3 forward = vec3(yawMatrix * vec4(0, 0, -1, 0));

    switch(event->key()) {
        case Qt::Key_W:
        velocity = vec3(0, velocity.y, 0);
            // forward
            break;
        case Qt::Key_A:
        velocity = vec3(0, velocity.y, 0);
            // left
            break;
        case Qt::Key_S:
        velocity = vec3(0, velocity.y, 0);
            // back
            break;
        case Qt::Key_D:
        velocity = vec3(0, velocity.y, 0);
            // right
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            break;
        case Qt::Key_Shift:
            shiftActive = false;
            break;
        case Qt::Key_Space:
            // up or jump
            break;
    }
}

void GLWidget::printMat4(mat4 mat) {
    double dArray[16] = {0.0};

    const float *pSource = (const float*) glm::value_ptr(mat);
    for (int i = 0; i < 16; ++i) {
        std::cout << pSource[i] << " ";
        if(i % 4 == 3)
            std::cout << std::endl;
    }
}

vec3 GLWidget::terrainPoint(int x, int z) {
    int verticalTerrainScale = 50;
    return vec3(terrainScale * (x + initTerrOffset.x), terrain->getHeight(x, z) * verticalTerrainScale + initTerrOffset.y, terrainScale * (z + initTerrOffset.z));
}

float GLWidget::getHeightAtLoc(float x, float z) {
    x = x / terrainScale - initTerrOffset.x;
    z = z / terrainScale - initTerrOffset.z;

    return GLWidget::terrainPoint(x, z).y;
}

float GLWidget::distanceToTowerCenter() {
    return sqrt(pow((position.x - towerOffset.x), 2) + pow((position.z - towerOffset.z), 2));
}

void GLWidget::animate() {
   // Timing Variables
   const float dt = .016f;
   time += dt;
   ticks++;

   /*
    * Model Animation Code
    */

   if(time > 2*(rotations.size()-1)) {
       time = 0;
   }

   float t = fmin(time/(2*(rotations.size()-1)),1);
//   float t2 = fmin(time/(2 * rotaitons.size()-1), 1);

//    Get two indices into our rotations array
//    that represent our current animation
   unsigned int fromIndex = t*(rotations.size()-1);
   unsigned int toIndex = fromIndex+1;

//    when t = 1 toIndex will be out of range, so
//    just clamp it to the last index in the array
   if(toIndex > rotations.size()-1) {
       toIndex = rotations.size()-1;
   }

   t = t * (rotations.size() - 1) - (int) (t * (rotations.size() - 1));

   // Euler angle representations of
   vec3 from = rotations[fromIndex];
   vec3 to = rotations[toIndex];

   quat fromQuat = quat(from);
   quat toQuat = quat(to);

   quat slerpQuat = glm::slerp(fromQuat, toQuat, t);
   animationMatrix = glm::toMat4(slerpQuat);

   quat antiSlerpQuat = glm::slerp(toQuat, fromQuat, t);
   antiClockWiseAnimationMatrix = glm::toMat4(antiSlerpQuat);

   /*
    * End Model Animation Code
    */

   /*
    * First Person Camera Animation Code
    */

   const float walkMod = 5;
   float runMod = 1;
   if(shiftActive) {
       runMod = 3;
   }

   float dist = distanceToTowerCenter();

   if(dist < 1) {
        gravity = vec3(0, .45f, 0);
        done = true;
   }

   if(done == true && position.y  > 90) {
       clearColor = vec3(.5f, .5f, 0.7f);
   }

   position += vec3(runMod * walkMod * velocity.x, velocity.y, runMod * walkMod * velocity.z) * dt;
   velocity += gravity;

   int xu = position.x / terrainScale - initTerrOffset.x;
   int zu = position.z / terrainScale - initTerrOffset.z;
   if(xu > 0 && xu < renderedPixels
           && zu > 0 && zu < renderedPixels) {
       float minHeadHeight = characterHeight + getHeightAtLoc(position.x, position.z);
       if(position.y < minHeadHeight) {
           position.y = minHeadHeight;
           velocity.y = 0;
       }
   } //else:: fall of the edge

   mat4 translationMatrix = glm::translate(mat4(1.0f), position);
   mat4 transform = translationMatrix * yawMatrix * pitchMatrix;

   viewMatrix = inverse(transform);

   /*
    * End First Person Camera Animation Code
    */

   glUseProgram(cubeProg);
   glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(viewMatrix));

   glUseProgram(terrProg);
   glUniformMatrix4fv(terrViewMatrixLoc, 1, false, value_ptr(viewMatrix));

   update();
}


//TODO: Put this into terrain class
Terrain* GLWidget::loadTerrain(const char* filename, float height) {
    Image* image = loadBMP(filename);
    Terrain* t = new Terrain(image->width, image->height);
    for(int y = 0; y < image->height; y++) {
        for(int x = 0; x < image->width; x++) {
            unsigned char color =
                (unsigned char)image->pixels[3 * (y * image->width + x)];
            float h = height * ((color / 255.0f));
            t->setHeight(x, y, h);
        }
    }

    delete image;
    t->computeNormals();

    return t;
}

