Features:

*Terrain generation based on heightmap .bmp file. This took me most of the time and unfortunately did not add much  to the final project. I decided to switch directions with the project
so the heightmap ended up being only trivially visable. You can notice that the heights change but only minisculy. Howwever, my code does work to import more detailed/extreme heightmaps,
and I plan to play with it more in the future. Currently, the player's height does change as he/she walks up and down the terrain, and you can fall off the edge.

*Gravity: Pressing the spacebar will cause the player to jump. There is a slight downwards accelleration that occurs in the integration step. This also comes into play later.

*Animation and color changing: As specified. In addition to the beginning box changing dimension without interpolated values, the centeral tower uses rotation quaternion interpolation.


Story:

Midway on our life's journey, I found myself
In dark woods, the right road lost. To tell
About those woods is hard--so tangled and rough

And savage that thinking of it now, I feel
The old fear stirring: death is hardly more bitter.
And yet, to treat the good I found there as well...

So opens Dante's Inferno. This is the text whose theme I attempted to reproduce in this project.

The player is first dropped into a slowly shrinking cube; something that might be found in Indiana Jones or the like. Althought obstacle collision has not yet been implemented, I envision the player not quite understanding what he/she is meant to do. It seems that they might be squished until a door appears at the last minute. This parrallels the first canto of Inferno, in which the protagonist is in a dark woods feeling as if his life is closing in around him, trapped and lost.

When he does emerge from these woods, his contracting box, the guidance (here: the door) appears beckoning him, paradoxically, to a manifistation of hell. Here this is represented by the rotatiing concentric towers, each of the 9 representing one of Dante's representations of Hell.

The player is meant to travel inwards: through the (strangely creepy, almost horrific) circles. Each grows more ominious, and though it was my goal, I did not expect for the feeling that I experience when traveling through these rings to quite feel so offputting.

However, when the player reaches the center of the inner tower, something happens that is meant again to mirror the path of Dante. Whether or not you go in for such pretentions, I think that it is accurate. The player is lifted out of the layers of hell into a more optimistic daylight, representing the salvation that is supposedly achieved by Dante after the conclusion of Inferno.

Note: If the terrain file is not able to be imported, make sure that it is in the same directory of the .exe.
