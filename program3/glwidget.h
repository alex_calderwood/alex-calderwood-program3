#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__
#define GLM_FORCE_RADIANS

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTimer>
#include <QStack>
#include "glwidget.h"
#include "terrain.h"

#define GLM_FORCE_RADIANS

using glm::mat4;
using glm::mat3;
using glm::vec3;
using glm::quat;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);

    public slots:
    // Part 2 - add an animate slot
        void animate();

    private:
        enum RenderMode {
            flat,
            doored,
            round,
            flue
        };

        void initializeCube();
        void GLWidget::initializeBrick();
        void GLWidget::renderCube();
        void GLWidget::renderBrick(float brickX, float brickY, float brickZ);
        void GLWidget::renderWall(RenderMode renderMode,
                                  int options,
                                  float brickX,
                                  float brickY,
                                  float brickZ,
                                  float offset,
                                  int numBricksX,
                                  int numBricksY);

        void GLWidget::renderFlatWall(int offsetConfig,
                                      float brickX,
                                      float brickY,
                                      float brickZ,
                                      float offset,
                                      int numBricksX,
                                      int numBricksY);

        void GLWidget::renderDooredWall(int offsetConfig,
                                      float brickX,
                                      float brickY,
                                      float brickZ,
                                      float offset,
                                      int numBricksX,
                                      int numBricksY);

        void GLWidget::renderCircle(float brickX,
                                    float brickY,
                                    float brickZ,
                                    float offset,
                                    int numBricksX,
                                    int numBricksY);

        void GLWidget::renderFlue(float brickX,
                                    float brickY,
                                    float brickZ,
                                    float offset,
                                    int numBricksX,
                                    int numBricksY);

        void GLWidget::renderBox();
        void GLWidget::renderTower();

        void GLWidget::translate(float, float, float);
        void GLWidget::rotate(float);
        void GLWidget::scale(float, float, float);
        void GLWidget::transform(mat4);
        void GLWidget::pop();

        void GLWidget::printMat4(mat4 mat);

        void GLWidget::initializeTerrain();
        void GLWidget::renderTerrain();
        Terrain* GLWidget::loadTerrain(const char* filename, float height);
        vec3 GLWidget::terrainPoint(int x, int y);
        float GLWidget::getHeightAtLoc(float x, float y);
        vec3 GLWidget::normal(int x, int y);
        int renderedPixels;

        float distanceToTowerCenter();

        void GLWidget::updateColors(float, float, float);

        float clrA;
        float clrS;
        float clrD;

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;
        GLint cubeAnimationMatrixLoc;

        GLint cubeLightPosLoc;
        GLint cubeLightColorLoc;
        GLint cubeLightIntensityLoc;

        GLint cubeDiffuseColorLoc;
        GLint cubeAmbientColorLoc;

        GLuint terrProg;
        GLuint terrVao;
        GLint terrProjMatrixLoc;
        GLint terrViewMatrixLoc;
        GLint terrModelMatrixLoc;

        GLint terrColorLoc;
        GLint terrainScale;
        vec3 initTerrOffset;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 animationMatrix;
        mat4 antiClockWiseAnimationMatrix;
        QStack<mat4> modelStack;

        int width;
        int height;

        QTimer* timer;
        float time;
        int ticks;

        bool done;

        std::vector<vec3> rotations;

        int curNumBricks;
        vec3 towerOffset;

        // First Person Stuff
        float characterHeight;

        mat4 pitchMatrix;
        mat4 yawMatrix;

        float pitchAngle;
        float yawAngle;

        vec3 position;
        vec3 velocity;
        vec3 gravity;

        vec3 clearColor;

        glm::vec2 lastPt;
        int shiftActive;

        Terrain* terrain;
};
#endif
