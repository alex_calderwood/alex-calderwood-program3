HEADERS += glwidget.h \ 
    imageloader.h \
    terrain.h
SOURCES += glwidget.cpp main.cpp \
    imageloader.cpp \
    terrain.cpp

QT += opengl
CONFIG -= app_bundle
CONFIG += console c++11
INCLUDEPATH += "../include"
INCLUDEPATH += $$PWD

RESOURCES += shaders.qrc
