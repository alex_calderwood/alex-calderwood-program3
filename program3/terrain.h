#ifndef TERRAIN_H
#define TERRAIN_H

#include <glm/glm.hpp>

#include "terrain.h"

using glm::vec3;

class Terrain {
public:
    Terrain::Terrain();
    Terrain::Terrain(int width, int length);
    Terrain::~Terrain();
    void Terrain::setHeight(int x, int z, float y);
    float Terrain::getHeight(int x, int z);
    vec3 Terrain::getNormal(int x, int z);
    void Terrain::computeNormals();
    int width;
    int length;

    private:
        float** heights;
        vec3** normals;
        bool computedNormals;
};
#endif
